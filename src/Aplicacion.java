import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.ColorDescriptor;
import org.eclipse.swt.graphics.RGB;

public class Aplicacion {

	protected Shell shell;
	private Text text;
	private LocalResourceManager localResourceManager;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Aplicacion window = new Aplicacion();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		createResourceManager();
		shell.setBackground(localResourceManager.create(ColorDescriptor.createFrom(new RGB(255, 128, 64))));
		shell.setSize(450, 300);
		shell.setText("SWT Application");
		
		Label lblNombre = new Label(shell, SWT.NONE);
		lblNombre.setForeground(localResourceManager.create(ColorDescriptor.createFrom(new RGB(255, 255, 255))));
		lblNombre.setBackground(localResourceManager.create(ColorDescriptor.createFrom(new RGB(255, 128, 64))));
		lblNombre.setBounds(10, 10, 55, 15);
		lblNombre.setText("Nombre");
		
		text = new Text(shell, SWT.BORDER);
		text.setBackground(localResourceManager.create(ColorDescriptor.createFrom(new RGB(128, 255, 128))));
		text.setBounds(71, 10, 76, 21);

	}
	private void createResourceManager() {
		localResourceManager = new LocalResourceManager(JFaceResources.getResources(),shell);
	}
}
